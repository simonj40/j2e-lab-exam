/**
 * Custom js to set and trigger the datepicker plugin
 */
$(function () {	
    $('#datepicker').datetimepicker({
    	format:"MM/DD/YYYY",
        defaultDate: moment().format("MM/DD/YYYY"),
        disabledDates: dates
    });
});