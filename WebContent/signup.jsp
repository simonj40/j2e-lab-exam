<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Sign Up</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
		  	<form class="form-signin" method="post" action="signup">
		    	<h2 class="form-signin-heading">Please Sign Up</h2>
		    	<label class="sr-only">First Name</label>
		    	<input name="fname" class="form-control" placeholder="Fisrt Name" required autofocus>
		    	<label class="sr-only">Last Name</label>
		    	<input name="lname" class="form-control" placeholder="Last Name" required>
		    	<label class="sr-only">Login</label>
		    	<input name="login" class="form-control" placeholder="Login" required>
		    	<label class="sr-only">Password</label>
		    	<input type="password" name="password" class="form-control" placeholder="Password" required>
		    	<button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
		    	<a href="index.jsp">Go to login page</a>
		  	</form>
		</div>
	</div>
</body>
</html>