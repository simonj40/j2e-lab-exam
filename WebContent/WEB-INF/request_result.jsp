<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Request Result</title>
</head>
<body>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<div class="jumbotron">
			<h2 class="form-signin-heading">New Leave</h2>
			<c:if test="${not empty errors}">
			    <p>Errors:</p>
			    <ul>
				    <c:forEach items="${errors}" var="error">
				        <li class="error" >${error}</li>
				    </c:forEach>
			    </ul>
			</c:if>
			<c:if test="${empty errors}">
				<p>A new leave has been created:</p>
				<ul>
					<li><em>Employee:</em> <c:out value="${user.fname}" /> <c:out value="${user.lname}" /> </li>
					<li><em>Date:</em> <c:out value="${leave.date}" /></li>
				</ul>
			</c:if>
			<a href='${goBack}'>Go Back</a>
		</div>
	</div>
</body>
</html>