<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>User Info</title>
</head>
<body>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<div class="jumbotron">
			<div class="row">
				<div class="col-md-6">
					<h2>User Info:</h2>
					<ul>
						<li><em>First Name:</em> <c:out value="${user.fname}" /></li>
						<li><em>Last Name:</em> <c:out value="${user.lname}" /></li>
						<li><em>Login:</em> <c:out value="${user.login}" /></li>
					</ul>
				</div>
				<div class="col-md-6">
					<h2>Leaves (total: ${fn:length(dates)})</h2>
					<div>
						<c:if test="${not empty dates}">
						    <ul>
							    <c:forEach items="${dates}" var="date">
							        <li><em>${date}</em></li>
							    </c:forEach>
						    </ul>
						</c:if>
						<c:if test="${empty dates}">
						    <p>No leaves have been set yet.</p>
						</c:if>
					</div>
					<a href="index" class="btn btn-primary">Create a new leave</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>