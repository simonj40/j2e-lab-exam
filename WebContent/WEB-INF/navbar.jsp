<div class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="userinfo">Logged in as: <c:out value="${user.fname}" /> <c:out value="${user.lname}" /></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="logout">Log Out <span class="glyphicon glyphicon-log-out"></span></a></li>
			</ul>
		</div>
	</div>
</div>