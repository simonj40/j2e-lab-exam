<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/site.js"></script>
<title>New Leave</title>
</head>
<body>
<script type="text/javascript">
var dates = [];
<c:if test="${not empty dates}">
    <c:forEach items="${dates}" var="date">dates.push("${date}");</c:forEach>
</c:if>
</script>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<div class="jumbotron">
			<h2 class="form-signin-heading">New Leave</h2>
			<div class="row">
				<form method="post" action="verification">
			        <div class='col-md-6'>
			        	<div class="form-group">
			        		<div class='input-group date' id='datepicker'>
			        			<input name="date" type='text' class="form-control" />
			       				<span class="input-group-addon">
			       					<span class="glyphicon glyphicon-calendar"></span>
			       				</span>	
			        		</div>
			        	</div>	
			        </div>
			        <div class='col-md-6'>
			            <button type="submit" class="btn btn-primary" action="submit">Create a new leave</button>
			        </div>
		        </form>
	  		</div>
		</div>
	</div>
</body>
</html>