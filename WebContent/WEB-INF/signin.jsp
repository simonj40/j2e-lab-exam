<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Sign In</title>
</head>
<body>
<div class="container">
	<div class="jumbotron">
  		<form class="form-signin" method="post" action="authentication">
    		<h2 class="form-signin-heading">Please sign in</h2>
    		<label class="sr-only">Login</label>
    		<input name="login" class="form-control" placeholder="Login" required autofocus>
    		<label class="sr-only">Password</label>
    		<input type="password" name="password" class="form-control" placeholder="Password" required>
    		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    		<a href="signup.jsp">Go to sign up page</a>
  		</form>
  	</div>
</div>
</body>
</html>