<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
			<c:if test="${not empty errors}">
			    <p>Errors:</p>
			    <ul>
				    <c:forEach items="${errors}" var="error">
				        <li class="error" >${error}</li>
				    </c:forEach>
			    </ul>
			</c:if>
			<c:if test="${empty errors}">
				<p>No Errors occured! :-)</p>
			</c:if>
			<a href='${goBack}'>Go Back</a>
		</div>
	</div>
</body>
</html>