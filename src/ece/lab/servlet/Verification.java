package ece.lab.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ece.lab.bean.Employee;
import ece.lab.bean.Leave;

/**
 * Servlet implementation class Verification
 */
@WebServlet("/Verification")
public class Verification extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Verification() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session =  request.getSession();
		String date = request.getParameter("date");
		Employee user = (Employee) session.getAttribute("user");
		if(user == null){
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		if(this.isDateValid(date)){
			Leave leave = new Leave(date, user.getIdemployee());
			if(leave.isAvailable()){
				if(leave.save()){
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("leave", leave);
		        	request.getSession().setAttribute("goBack", "index");
		        	request.getRequestDispatcher("/WEB-INF/request_result.jsp").forward(request, response);
				}else{
					request.getSession().setAttribute("errors", leave.getErrors());
		        	request.getSession().setAttribute("goBack", "index");
		        	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
		        	request.getSession().removeAttribute("errors");
				}
			}else{
				request.getSession().setAttribute("errors", leave.getErrors());
	        	request.getSession().setAttribute("goBack", "index");
	        	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
	        	request.getSession().removeAttribute("errors");
			}
			
			
		}
	}
	
	private boolean isDateValid(String input){
		SimpleDateFormat format = new SimpleDateFormat("MM/DD/YYYY");
		try {
	          format.parse(input);
	          return true;
	     }
	     catch(Exception e){
	          return false;
	     }
	}

}
