package ece.lab.servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import javax.servlet.ServletContextEvent;

import ece.lab.db.DB;

public class StartupListener implements javax.servlet.ServletContextListener  {
	
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		//Load SQLite jdbc driver
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			System.out.println("DRIVER LOADED !!!!");
			//DataBase creation
			c = DB.getConnection();
			c.setAutoCommit(false);
			Statement stmt = c.createStatement();
			stmt.executeUpdate(DB.SQL1);
			stmt.executeUpdate(DB.SQL2);
			stmt.close();
			c.commit();
		    c.close();
		    System.out.println("DATABASE CREATED");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
