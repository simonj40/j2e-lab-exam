package ece.lab.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ece.lab.bean.Employee;
import ece.lab.bean.Leave;

/**
 * Servlet implementation class Authentication
 */
@WebServlet("/Authentication")
public class Authentication extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int maxSessionAge = 50;
	private static final int maxCookieAge = 60*60*24*4;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Authentication() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee user = this.checkCookies(request);
		if(user == null){
			user = this.checkSession(request);
		}
		if(user != null){
			//update session
			HttpSession session =  request.getSession();
			session.setAttribute("time", System.currentTimeMillis());
			session.setAttribute("user", user);
			//update cookies
			Cookie loginCookie = this.getLoginCookie(request);
			Cookie passwordCookie = this.getPasswordCookie(request);
			if(loginCookie == null){
				loginCookie = new Cookie("login", user.getLogin());
			}
			if(passwordCookie == null){
				passwordCookie = new Cookie("password", user.getPassword());
			}
			loginCookie.setMaxAge(maxCookieAge);
        	passwordCookie.setMaxAge(maxCookieAge);
        	response.addCookie(loginCookie);
        	response.addCookie(passwordCookie);
        	//get existing leaves dates
        	Leave leave = new Leave();
        	ArrayList<String> dates = leave.getLeaves();
        	if(dates != null){
        		request.getSession().setAttribute("dates", dates);
            	request.getRequestDispatcher("/WEB-INF/leave_request.jsp").forward(request, response);
        	}else{
        		request.getSession().setAttribute("erros",leave.getErrors());
        		request.getSession().setAttribute("goBack", "index");
            	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
            	request.getSession().removeAttribute("errors");
        	}			
		}else{
			request.getRequestDispatcher("/WEB-INF/signin.jsp").forward(request, response);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee user = new Employee();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        user.setLogin(login);
        user.setPassword(password);
        if(user.login()){
        	System.out.println("USER AUTHENIFIED!");
        	//set session
        	HttpSession session =  request.getSession();
        	session.setAttribute("user", user);
        	session.setAttribute("time", System.currentTimeMillis());
        	//set cookie
        	Cookie loginCookie = new Cookie("login", user.getLogin());
        	Cookie passwordCookie = new Cookie("password", user.getPassword());
        	loginCookie.setMaxAge(maxCookieAge);
        	passwordCookie.setMaxAge(maxCookieAge);
        	response.addCookie(loginCookie);
        	response.addCookie(passwordCookie);
        	request.getSession().setAttribute("user", user);
        	//get existing leaves dates
        	Leave leave = new Leave();
        	ArrayList<String> dates = leave.getLeaves();
        	if(dates != null){
        		request.getSession().setAttribute("dates", dates);
            	request.getRequestDispatcher("/WEB-INF/leave_request.jsp").forward(request, response);
        	}else{
        		request.getSession().setAttribute("erros",leave.getErrors());
        		request.getSession().setAttribute("goBack", "index");
            	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
            	request.getSession().removeAttribute("errors");
        	}
        }else{
        	System.out.println("ERRORS: "+user.getErrors().toString());
        	request.getSession().setAttribute("errors", user.getErrors());
        	request.getSession().setAttribute("goBack", "index");
        	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
        	request.getSession().removeAttribute("errors");
        }
	}
	
	private Employee checkCookies(HttpServletRequest request){
		Employee user = null;
		String login = null;
		String password = null;	
		Cookie loginCookie = this.getLoginCookie(request);
		Cookie passwordCookie = this.getPasswordCookie(request);
		if(loginCookie!=null && passwordCookie!=null){
			login = loginCookie.getValue();
			password =  passwordCookie.getValue();
			if(login!=null && password!=null){
				user = new Employee(login, password);
				if(user.login()){
					return user;
				}
			}else{
				return null;
			}
		}else{
			return null;
		}
		return null;
	}
	
	private Cookie getLoginCookie(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies !=null){
			for(Cookie cookie : cookies){
			    if(cookie.getName().equals("login")){
			    	return cookie;
			    }			    
			}
		}
		return null;
	}
	private Cookie getPasswordCookie(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies !=null){
			for(Cookie cookie : cookies){
			    if(cookie.getName().equals("password")){
			    	return cookie;
			    }			    
			}
		}
		return null;
	}
	
	private Employee checkSession(HttpServletRequest request){
		HttpSession session =  request.getSession();
		Long time = (Long) session.getAttribute("time");
		Employee user = (Employee) session.getAttribute("user");
		if(time != null && user != null){
			if(System.currentTimeMillis() - time.longValue() < maxSessionAge*1000){
				return user;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
}
