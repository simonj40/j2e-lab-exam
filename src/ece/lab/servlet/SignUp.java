package ece.lab.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ece.lab.bean.Employee;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee user = new Employee();
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        user.setFname(fname);
        user.setLname(lname);
        user.setLogin(login);
        user.setPassword(password);
        if(user.save()){
        	System.out.println("USER SAVED OK");
        	request.getSession().setAttribute("user", user);
        	request.getRequestDispatcher("/index.jsp").forward(request, response);
        }else{
        	System.out.println("ERRORS: "+user.getErrors().toString());
        	request.getSession().setAttribute("errors", user.getErrors());
        	request.getSession().setAttribute("goBack", "signup");
        	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
        	request.getSession().removeAttribute("errors");
        }
	}
}
