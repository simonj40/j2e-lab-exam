package ece.lab.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ece.lab.bean.Employee;
import ece.lab.bean.Leave;

/**
 * Servlet implementation class UserInfo
 */
@WebServlet("/UserInfo")
public class UserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session =  request.getSession();
		Employee user = (Employee) session.getAttribute("user");
		if(user == null){
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}else{
			Leave leave = new Leave();
			ArrayList<String> dates = leave.getLeaves(user.getIdemployee());
			
			if(dates != null){
	    		request.getSession().setAttribute("dates", dates);
	    		request.getSession().setAttribute("user", user);
	        	request.getRequestDispatcher("/WEB-INF/user.jsp").forward(request, response);
	    	}else{
	    		request.getSession().setAttribute("erros",leave.getErrors());
	    		request.getSession().setAttribute("goBack", "index");
	        	request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
	        	request.getSession().removeAttribute("errors");
	    	}
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
