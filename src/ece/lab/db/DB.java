/**
 * 
 */
package ece.lab.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Simon
 *
 */
public final class DB {

	public static String DB = "jdbc:sqlite:"+System.getProperty("user.home")+"/app.db";
	
	public static String SQL1 =	"CREATE TABLE IF NOT EXISTS `employee` ("+
					"`idemployee` INTEGER PRIMARY KEY AUTOINCREMENT,"+
					"`fname` TEXT NOT NULL,"+
					"`lname` TEXT NOT NULL,"+
					"`login` TEXT NOT NULL UNIQUE,"+
					"`password` TEXT NOT NULL);";
	
	public static String SQL2 = 	"CREATE TABLE IF NOT EXISTS `leave` ("+
				  	"`idconge` INTEGER PRIMARY KEY AUTOINCREMENT,"+
				  	"`date` TEXT NOT NULL UNIQUE,"+
				  	"`idemployee` INTEGER NOT NULL,"+
				  	"FOREIGN KEY(idemployee) REFERENCES employee(idemployee));";

	public static Connection getConnection() throws SQLException{
		return DriverManager.getConnection(DB);
	}
		
	
	public DB() {
	}
}
