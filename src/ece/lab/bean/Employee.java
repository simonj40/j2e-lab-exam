/**
 * 
 */
package ece.lab.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ece.lab.db.DB;

/**
 * @author Simon
 *
 */
public class Employee {
	private int idemployee;
	private String fname;
	private String lname;
	private String login;
	private String password;
	
	private ArrayList<String> errors =  new ArrayList<String>();
	
	private String insert_query = "INSERT INTO employee"
			+ "(fname, lname, login, password) VALUES"
			+ "(?,?,?,?)";
	
	private String login_query = "SELECT * FROM employee WHERE "
			+ "login = ? ;";
		
	/**
	 * @param login
	 * @param password
	 */
	public Employee(String login, String password) {
		this.login = login;
		this.password = password;
	}
	public Employee() {
		super();
	}

	public boolean save(){
		Connection c = null;
		PreparedStatement stmt = null;
		try {
			c = DB.getConnection();
			stmt = c.prepareStatement(this.insert_query);
			stmt.setString(1, this.fname);
			stmt.setString(2, this.lname);
			stmt.setString(3, this.login);
			stmt.setString(4, this.password);
			stmt.executeUpdate();
			System.out.println("EMPLOYEE SAVED");
		} catch (Exception e) {
			e.printStackTrace();
			errors.add(e.getMessage());
			return false;
		} finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
				return false;
			}
		}
		return true;
	}
	
	public boolean login(){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		try {
			c = DB.getConnection();
			stmt = c.prepareStatement(this.login_query);
			stmt.setString(1, this.login);
			result = stmt.executeQuery();
			if(result.next()){
				if(this.password.equals(result.getString("password"))){
					this.idemployee = result.getInt("idemployee");
					this.fname = result.getString("fname");
					this.lname = result.getString("lname");
					return true;
				}else{
					this.errors.add("Wrong credentials");
					return false;
				}
				
			}else{
				this.errors.add("User doesnt' exist");
				return false;
			}
		} catch (Exception e) {
			errors.add(e.getMessage());
			return false;
		} finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
				return false;
			}
		}
	}
	
	public ArrayList<String> getErrors(){
		return this.errors;
	}
	
	/**
	 * @return the idemployee
	 */
	public int getIdemployee() {
		return idemployee;
	}
	/**
	 * @param idemployee the idemployee to set
	 */
	public void setIdemployee(int idemployee) {
		this.idemployee = idemployee;
	}
	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}
	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}
	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}
	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
