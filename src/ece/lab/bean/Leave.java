/**
 * 
 */
package ece.lab.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ece.lab.db.DB;

/**
 * @author Simon
 *
 */
public class Leave {
	private int idconge;
	private String date;
	private int idemployee;
	
	private ArrayList<String> errors =  new ArrayList<String>();
		
	private String available_date_query = "SELECT * FROM leave WHERE "
			+ "date = ? ;";
	private String insert_query = "INSERT INTO leave"
			+ "(date, idemployee) VALUES"
			+ "(?,?)";
	private String select_leaves_query = "SELECT * FROM leave;";
	private String select_employee_leaves_query = "SELECT * FROM leave WHERE "
			+ "idemployee = ? ;";
	
	public boolean save(){
		Connection c = null;
		PreparedStatement stmt = null;
		try {
			c = DB.getConnection();
			stmt = c.prepareStatement(this.insert_query);
			stmt.setString(1, this.date);
			stmt.setInt(2, this.idemployee);
			stmt.executeUpdate();
			System.out.println("LEAVE SAVED");
		} catch (Exception e) {
			e.printStackTrace();
			errors.add(e.getMessage());
			return false;
		} finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
				return false;
			}
		}
		return true;
	}
	
	public boolean isAvailable(){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		if(date != null){
			try {
				c = DB.getConnection();
				stmt = c.prepareStatement(this.available_date_query);
				stmt.setString(1, this.date);
				result = stmt.executeQuery();
				if(result.next()){
					errors.add("this date is not available");
					return false;
				}else{
					return true;
				}
			} catch (Exception e) {
				errors.add(e.getMessage());
				return false;
			} finally {
				try {
					if(stmt != null) {
						stmt.close();
					}
					if (c != null) {
						c.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
					errors.add(e.getMessage());
					return false;
				}
			}
		}else{
			errors.add("date provided is null");
			return false;
		}
	}
	
	public ArrayList<String> getLeaves(){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		ArrayList<String> leaves = new ArrayList<String>();
		try {
			c = DB.getConnection();
			stmt = c.prepareStatement(select_leaves_query);
			result = stmt.executeQuery();
			while(result.next()){
				leaves.add(result.getString("date"));
			}
		} catch (Exception e) {
			errors.add(e.getMessage());
			return null;
		} finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
				return null;
			}
		}
		return leaves;
	}
	
	public ArrayList<String> getLeaves(int id){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		ArrayList<String> leaves = new ArrayList<String>();
		try {
			c = DB.getConnection();
			stmt = c.prepareStatement(select_employee_leaves_query);
			stmt.setInt(1, id);
			result = stmt.executeQuery();
			while(result.next()){
				leaves.add(result.getString("date"));
			}
		} catch (Exception e) {
			errors.add(e.getMessage());
			return null;
		} finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
				return null;
			}
		}
		return leaves;
	}
	
	public ArrayList<String> getErrors(){
		return this.errors;
	}
	
	/**
	 * 
	 */
	public Leave() {
		super();
	}
	/**
	 * @param date
	 * @param idemployee
	 */
	public Leave(String date, int idemployee) {
		super();
		this.date = date;
		this.idemployee = idemployee;
	}
	/**
	 * @return the idconge
	 */
	public int getIdconge() {
		return idconge;
	}
	/**
	 * @param idconge the idconge to set
	 */
	public void setIdconge(int idconge) {
		this.idconge = idconge;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the idemployee
	 */
	public int getIdemployee() {
		return idemployee;
	}
	/**
	 * @param idemployee the idemployee to set
	 */
	public void setIdemployee(int idemployee) {
		this.idemployee = idemployee;
	}
	
	
	

}
